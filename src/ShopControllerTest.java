import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.lang.Math;
import java.util.*;

/**
 * The test class ShopControllerTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class ShopControllerTest
{
    /**
     * Default constructor for test class ShopControllerTest
     */
    public int totalNum;
    public Hotel[] h;
    public Customer tester;
    public MyModel backend;
    public Date s;
    public Date t;
    public float[] singlePrice;
    public float[] standardPrice;
    public float[] familyPrice;
    public int[] singleNum;
    public int[] standardNum;
    public int[] familyNum;

    ShopController shop;

    public ShopControllerTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */

    @Before
    public void setUp()
    {
      totalNum = 2;
      h = new Hotel[totalNum];
      singlePrice = new float[]{10, 100};
      standardPrice = new float[]{20, 200};
      familyPrice = new float[]{30, 300};
      singleNum = new int[]{4, 4};
      standardNum = new int[]{4, 4};
      familyNum = new int[]{4, 4};
      backend = new MyModel("testData");
      shop = new ShopController(backend) {
        public void setView(View view) {}
        public void showPopup(String message) {}
      };
      s = new Date();
      t = new Date(s.getTime() + (3600*1000*24));

      tester = new Customer("tester", "tester", "tester", "caulfield", "123", "456");
      backend.passwords.put("tester", "tester");
      backend.details.put("tester", tester);
      shop.attemptLogin("tester", "tester", "customer");

      for (int i=0; i<totalNum; i++) {
        String hName = String.format("hotel%d", i);
        h[i] = backend.hotels.get(backend.addHotel(hName, "", singlePrice[i], standardPrice[i], familyPrice[i]));
        h[i].addRooms("single", singleNum[i]);
        h[i].addRooms("standard", standardNum[i]);
        h[i].addRooms("family", familyNum[i]);
      }
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown() { }

    @Test
    public void testAddCartPos() {
      int id = 0;
      int num = 1;
      shop.addToCart(h[id], num, s, t, "single");
      int size = tester.cart.getList().size();
      assertEquals(size, num);
      System.out.printf("Correct, cart size is %d\n", size);
    }

    @Test
    public void testAddCartNeg() {
      int id = 1;
      int num = singleNum[id] + 10000;
      shop.addToCart(h[id], num, s, t, "single");
      int size = tester.cart.getList().size();
      assertEquals(size, singleNum[id]);
      System.out.printf("Correct, cart size is %d\n", size);
    }

    @Test
    public void testPriceCount() {
      float sum = 0;
      for (int i=0; i<totalNum; i++) {
        shop.addToCart(h[i], 1, s, t, "single");
        sum += singlePrice[i];
      }
      float price = shop.getTotalCartPrice();
      assertTrue(Math.abs(price - sum) < 1e-6);
      System.out.printf("Correct, total price is %f\n", price);
    }

    @Test
    public void testSearchAvailableItems1() {
      ArrayList<Hotel> res = backend.getHotels(s, t, "", false);
      assertEquals(res.size(), totalNum);
      System.out.printf("Correct, total items is %d\n", totalNum);
    }

    @Test
    public void testSearchAvailableItems2() {
      shop.addToCart(h[0], singleNum[0], s, t, "single");
      shop.addToCart(h[0], standardNum[0], s, t, "standard");
      shop.addToCart(h[0], familyNum[0], s, t, "family");

      ArrayList<Hotel> res = backend.getHotels(s, t, "", false);
      assertEquals(res.size(), totalNum-1);
      System.out.printf("Correct, total items is %d\n", totalNum-1);
    }


    @Test
    public void testCreateOrder() {
      for (int i=0; i<totalNum; i++) {
        shop.addToCart(h[i], singleNum[i], s, t, "single");
        shop.addToCart(h[i], standardNum[i], s, t, "standard");
        shop.addToCart(h[i], familyNum[i], s, t, "family");
        shop.attemptTransaction();
      }
      int payCnt = tester.paymentIDs.size();
      assertEquals(payCnt, totalNum);
      System.out.printf("Correct, total payments is %d\n", payCnt);
    }
}
