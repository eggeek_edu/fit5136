import java.util.*;

public class Apartment extends Accommodation {

  private int kitchenNum = 1;

  public Apartment(String acID, String acName, String address) { super(acID, acName, address); }

  public int getKitchenNum() { return kitchenNum; }
  public void setKitchenNum(int k) { kitchenNum = k; }
  public String toHtml(Date s, Date t, String style) {
    ArrayList<String> out = new ArrayList<>();
    out.add("<html>");
    out.add(String.format("<b>Kitchen Num: %d</b><br>", kitchenNum));
    out.add(super.toHtml(s, t, style));
    out.add("</html>");
    return String.join("", out);
  }
}
