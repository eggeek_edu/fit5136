import java.util.*;

public class Hotel extends Accommodation {
  private int starLevel = 5;

  public Hotel(String acID, String acName, String address) {
    super(acID, acName, address);
  }

  public int getStarLevel() { return starLevel; }
  public void setStarLevel(int sl) { starLevel = sl; }
  public String toHtml(Date s, Date t, String style) {
    ArrayList<String> out = new ArrayList<>();
    out.add("<html>");
    out.add(String.format("<b>Star Level: %d</b><br>", starLevel));
    out.add(super.toHtml(s, t, style));
    out.add("</html>");
    return String.join("", out);
  }
}
