import java.io.Serializable;
import java.util.*;

public class Order implements Serializable {
  public String orderID;
  public String customerID;
  public ArrayList<String> itemIDs;
  public float amount;
  public String paymentID;
  public String refundID;
  public Date createDate;
  private String status;

  public Order(String orderID, String customerID, ArrayList<String> itemIDs, float amount) {
    this.orderID = orderID;
    this.customerID = customerID;
    this.status = "pending";
    this.amount = amount;
    this.itemIDs = new ArrayList<>(itemIDs);
    this.createDate = new Date();
  }

  public String getStatus() { return status; }
  public PaymentRecord setFinish(String paymentID) {
    this.status = "finish";
    this.paymentID = paymentID;
    PaymentRecord res = new PaymentRecord(paymentID, createDate,
      new Date(), amount);
    return res;
  }
}
