import java.io.Serializable;
import java.util.*;

/**
 * <pre>
 * This class represents a shopping cart, the place where the selected items are held before they are purchased.
 * </pre>
 */
public class Cart implements Serializable {

    private ArrayList<CartItem> items = new ArrayList<>();

    public void add(MyModel b, Accommodation p, String roomID, Date s, Date t) {
      CartItem item = new CartItem(Integer.toString(b.nextItemID()), p, roomID, s, t);
      b.items.put(item.itemID, item);
      //System.out.printf("add CartItem, acID: %s, itemID: %s, roomID: %s\n", item.acID, item.itemID, item.roomID);
      add(item);
    }

    /**
     * <pre>
     * Adds a pre-formatted CartItem to the cart.
     * Preferably use add(Product, float) as that method performs checks to prevent duplicate items.
     * </pre>
     * @param item The CartItem to add
     */
    public void add(CartItem item) {
        items.add(item);
    }

    /**
     * <pre>
     * Returns a list of all CartItems.
     * </pre>
     * @return The list of CartItems
     */
    public List<CartItem> getList(){
        return items;
    }

    /**
     * <pre>
     * Removes all items from this cart.
     * </pre>
     */
    public void clear(){
        items.clear();
    }

    /**
     * <pre>
     * Replaces the list of items with a different one.
     * </pre>
     * @param items The new list of items.
     */
    public void setItems(ArrayList<CartItem> items) {
        this.items = items;
    }

    /**
     * <pre>
     * Removes a specific item from the cart.
     * </pre>
     * @param item The item to remove
     */
    public void remove(CartItem item){
      if (item.accommodation != null) {
        Room r = item.accommodation.getRoomByID(item.roomID);
        r.unbook(item.s, item.t);
      }
      items.remove(item);
    }

    public void removeAll() {
      for (CartItem i: items) {
        Room r = i.accommodation.getRoomByID(i.roomID);
        r.unbook(i.s, i.t);
      }
      items.clear();
    }
}
