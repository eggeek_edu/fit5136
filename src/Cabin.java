import java.util.*;

public class Cabin extends Accommodation {
  private int gardenNum = 1;
  private String landscape = "mountain";

  public Cabin(String acID, String acName, String address) { super(acID, acName, address); }

  public int getGardenNum() { return gardenNum; }
  public String getLandscape() { return landscape; }
  public void setGardenNum(int num) { gardenNum = num; }
  public void setLandscape(String landscape) {this.landscape = landscape; }

  public String toHtml(Date s, Date t, String style) {
    ArrayList<String> out = new ArrayList<>();
    out.add("<html>");
    out.add(String.format("<b>Landscape: %s</b><br>", landscape));
    out.add(String.format("<b>Garden Num: %d</b><br>", gardenNum));
    out.add(super.toHtml(s, t, style));
    out.add("</html>");
    return String.join("", out);
  }
}
