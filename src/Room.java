import java.io.Serializable;
import java.util.*;

public class Room implements Serializable {
  private String acID;
  private String acName;
  private String roomID;
  private HashSet<DateRange> bookedDates;
  private float price;

  public Room(String acID, String acName) {
    this.acID = acID;
    this.acName = acName;
    this.bookedDates = new HashSet<DateRange>();
  }

  public String getAcID() { return acID; }
  public String getAcName() { return acName; }
  public String getRoomID() { return roomID; }
  public void setRoomID(String roomID) { this.roomID = roomID; }
  public ArrayList<DateRange> bookedList() {
    ArrayList<DateRange> res = new ArrayList<DateRange>();
    for (DateRange i: bookedDates) res.add(i);
    return res;
  }
  public boolean isAvailable(Date s, Date t) {
    DateRange dr = new DateRange(s, t);
    for (DateRange i: bookedDates) if (i.isCollide(dr)) return false;
    return true;
  }
  public void book(Date s, Date t) {
    DateRange dr = new DateRange(s, t);
    bookedDates.add(dr);
  }
  public float getPrice() { return price; }
  public void setPrice(float p) { this.price = p; }
  public void unbook(Date s, Date t) {
    DateRange dr = new DateRange(s, t);
    bookedDates.remove(dr);
  }
}
