import java.util.*;

public class PaymentRecord extends Transaction {
  public String paymentID;
  public PaymentRecord(String paymentID, Date s, Date t, float amount) {
    super(s, t, amount);
    this.paymentID = paymentID;
  }
}
