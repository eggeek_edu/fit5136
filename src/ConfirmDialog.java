import java.util.*;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class ConfirmDialog extends JDialog {

    private static final long serialVersionUID = 1L;

    public static void display(ShopController c) {
        ConfirmDialog dialog = new ConfirmDialog(c);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(c.getWindow());
        dialog.setVisible(true);
    }

    public ConfirmDialog(ShopController c) {
        JDialog me = this;
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        {
            JPanel panel = new JPanel();
            panel.setBorder(new EmptyBorder(10, 10, 10, 10));
            getContentPane().add(panel, BorderLayout.CENTER);
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            {
                JLabel lblNewLabel = new JLabel("ORDER DETAILS:");
                panel.add(lblNewLabel);
            }
            {
                JLabel spacer = new JLabel("  ");
                panel.add(spacer);
            }
            {
                String text = "<html>";
                String foo = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

                for(CartItem item : c.getCart().getList()){
                    Cart thisItemInACart = new Cart();
                    thisItemInACart.add(item);
                    float price = c.getBackend().getPrice(thisItemInACart);
                    text += "ITEM: " + item.accommodation.getAcName() +
                            String.format("%sstart: %s", foo, df.format(item.s)) +
                            String.format("%send: %s", foo, df.format(item.t)) +
                            String.format("%sPRICE: %.2f", foo,  price) + "<br>";
                }

                text += "</html>";
                JLabel details = new JLabel(text);
                panel.add(details);
            }
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton confirmButton = new JButton("Confirm order");
                confirmButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        c.attemptTransaction();
                        me.dispose();
                    }
                });
                confirmButton.setActionCommand("OK");
                buttonPane.add(confirmButton);
                getRootPane().setDefaultButton(confirmButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        me.dispose();
                    }
                });
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }
}
