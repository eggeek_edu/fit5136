import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;

public class AccommodationDetails extends JDialog {

  private static final long serialVersionUID = 1L;

  private JPanel contentPanel = new JPanel();
  private JLabel reportLabel = new JLabel();
  private JScrollPane scr;
  private String curStyle;
  JSpinner spinner;
  JLabel lblNewLabel_2;

  public static void display(ShopController c, Accommodation p, Date s, Date t, String style) {
    AccommodationDetails dialog = new AccommodationDetails(c, p, s, t, style);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setLocationRelativeTo(c.getWindow());
    dialog.setVisible(true);
  }

  public void createContent(ShopController c, Accommodation p, Date s, Date t, String style, JPanel content) {
    {
      JPanel panel = new JPanel();
      panel.setBorder(new EmptyBorder(10, 10, 10, 10));
      content.add(panel);
      panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
      {
        JLabel lblNewLabel_3 = new JLabel();
        lblNewLabel_3.setIcon(p.getImage());
        panel.add(lblNewLabel_3);
      }
    }
    {
      JPanel panel = new JPanel();
      content.add(panel);
      panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
      {
        JPanel panel_1 = new JPanel();
        panel.add(panel_1);
        panel_1.setBorder(null);
        FlowLayout fl_panel_1 = (FlowLayout) panel_1.getLayout();
        fl_panel_1.setAlignment(FlowLayout.LEFT);
        {
          JLabel lblNewLabel_1 = new JLabel(p.getAcName());
          panel_1.add(lblNewLabel_1);
          lblNewLabel_1.setHorizontalAlignment(SwingConstants.LEFT);
          lblNewLabel_1.setFont(new Font("Lucida Grande", Font.PLAIN, 20));
        }
      }
      {
        JPanel panel_1 = new JPanel();
        panel.add(panel_1);
        FlowLayout fl_panel_1 = (FlowLayout) panel_1.getLayout();
        fl_panel_1.setAlignment(FlowLayout.LEFT);
        panel_1.setBorder(new EmptyBorder(10, 10, 10, 10));
        {
          lblNewLabel_2 = new JLabel(p.toHtml(s, t, style));
          panel_1.add(lblNewLabel_2);
        }
      }
    }
  }

  private void createOptions(ShopController c, Accommodation p, Date s, Date t, String style, JPanel options) {
    options.add(new JLabel("choose style:"));
    JButton single = new JButton("single");
    if (p.styleAvailableNum(s, t, "single") == 0) single.setEnabled(false);
    single.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        lblNewLabel_2.setText(p.toHtml(s, t, "single"));
        curStyle = "single";
        spinner.setModel(new SpinnerNumberModel(1, 1, p.styleAvailableNum(s, t, curStyle), 1));
      }
    });
    options.add(single);

    JButton standard = new JButton("standard");
    if (p.styleAvailableNum(s, t, "standard") == 0) standard.setEnabled(false);
    standard.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        lblNewLabel_2.setText(p.toHtml(s, t, "standard"));
        curStyle = "standard";
        spinner.setModel(new SpinnerNumberModel(1, 1, p.styleAvailableNum(s, t, curStyle), 1));
      }
    });
    options.add(standard);

    JButton family = new JButton("family");
    if (p.styleAvailableNum(s, t, "family") == 0) family.setEnabled(false);
    family.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        lblNewLabel_2.setText(p.toHtml(s, t, "family"));
        curStyle = "family";
        spinner.setModel(new SpinnerNumberModel(1, 1, p.styleAvailableNum(s, t, curStyle), 1));
      }
    });
    options.add(family);
  }

  private void createActions(ShopController c, Accommodation p, Date s, Date t, String style, JPanel actions) {
    actions.setLayout(new FlowLayout(FlowLayout.RIGHT));
    {
      JLabel lblNewLabel = new JLabel("Quantity:");
      actions.add(lblNewLabel);
    }
    {
      spinner = new JSpinner();
      spinner.setModel(new SpinnerNumberModel(1, 1, p.styleAvailableNum(s, t, curStyle), 1));
      spinner.setPreferredSize(new Dimension(100, 30));
      actions.add(spinner);
    }
    {
      JDialog me = this;
      JButton okButton = new JButton("Add to cart");
      okButton.setActionCommand("OK");
      actions.add(okButton);
      okButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e) {
          c.addToCart(p, (int)spinner.getModel().getValue(), s, t, curStyle);
          me.dispose();
        }
      });
      getRootPane().setDefaultButton(okButton);
    }
    {
      JDialog me = this;
      JButton cancelButton = new JButton("Cancel");
      cancelButton.setActionCommand("Cancel");
      cancelButton.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e) {
          me.dispose();
        }
      });
      actions.add(cancelButton);
    }
  }

  private void setUpCustomerPanel(ShopController c, Accommodation p, Date s, Date t, String style) {
    getContentPane().add(contentPanel, BorderLayout.NORTH);
    contentPanel.setLayout(new BorderLayout(0, 0));

    JPanel options = new JPanel();
    createOptions(c, p, s, t, style, options);
    contentPanel.add(options, BorderLayout.NORTH);

    JPanel actions = new JPanel();
    createActions(c, p, s, t, style, actions);
    contentPanel.add(actions, BorderLayout.SOUTH);

    JPanel content = new JPanel();
    createContent(c, p, s, t, style, content);
    contentPanel.add(content, BorderLayout.CENTER);
  }

  private void createReportTopPanel(ShopController c, Accommodation p, Date s, Date t) {
    JPanel options = new JPanel();
    options.setLayout(new FlowLayout());
    options.add(new JLabel("choose style:"));
    JButton single = new JButton("single");
    JButton standard = new JButton("standard");
    JButton family = new JButton("family");

    if (p.styleBookedNum(s, t, "single") == 0) single.setEnabled(false);
    if (p.styleBookedNum(s, t, "standard") == 0) standard.setEnabled(false);
    if (p.styleBookedNum(s, t, "family") == 0) family.setEnabled(false);

    single.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        reportLabel.setText(p.toReportHTML(c.getBackend(), s, t, "single"));
      }
    });

    standard.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        reportLabel.setText(p.toReportHTML(c.getBackend(), s, t, "standard"));
      }
    });

    family.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        reportLabel.setText(p.toReportHTML(c.getBackend(), s, t, "family"));
      }
    });
    options.add(single);
    options.add(standard);
    options.add(family);
    add(options, BorderLayout.NORTH);
  }

  private void createReportCentralPanel(ShopController c, Accommodation p, Date s, Date t, String style) {
    scr = new JScrollPane(contentPanel);
    scr.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scr.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    contentPanel.setLayout(new BorderLayout());
    contentPanel.add(reportLabel, BorderLayout.CENTER);
    reportLabel.setText(p.toReportHTML(c.getBackend(), s, t, style));
    add(scr, BorderLayout.CENTER);
  }

  private void createReportBottomPanel() {
    JPanel bottom = new JPanel(new FlowLayout());
    JButton ok = new JButton("OK");
    bottom.add(ok, BorderLayout.EAST);
    ok.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    add(bottom, BorderLayout.SOUTH);
  }

  private void setUpAdminPanel(ShopController c, Accommodation p, Date s, Date t, String style) {
    createReportTopPanel(c, p, s, t);
    createReportCentralPanel(c, p, s, t, style);
    createReportBottomPanel();
  }

  public AccommodationDetails(ShopController c, Accommodation p, Date s, Date t, String style) {
    this.curStyle = style;
    setBounds(100, 100, 450, 400);
    getContentPane().setLayout(new BorderLayout());
    contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
    if (c.currentUserType.equals("customer")) {
      setUpCustomerPanel(c, p, s, t, style);
    } else {
      setUpAdminPanel(c, p, s, t, style);
    }
  }
}
