import java.util.*;

public class RefundRecord extends Transaction {
  public String refundID;

  public RefundRecord(String refundID, Date s, Date t, float amount) {
    super(s, t, amount);
    this.refundID = refundID;
  }
}
