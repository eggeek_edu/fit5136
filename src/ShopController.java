import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import java.awt.event.*;

public class ShopController {

    /*
     * STATIC FIELDS
     */

    /**
     * <pre>
     * The default product icon.
     * </pre>
     */
    public static ImageIcon NO_IMAGE_ICON = generateIcon("https://placeholdit.imgix.net/~text?txtsize=23&bg=ffffff&txtclr=000000&txt=No+Image&w=200&h=200", 150, 150);
    /**
     * <pre>
     * The store logo.
     * </pre>
     */
    public static ImageIcon LOGO_ICON = new ImageIcon(ShopController.class.getResource("logo.png"));
    /**
     * <pre>
     * The image cache that is used to save time and speed up loading.
     * </pre>
     */
    public static HashMap<String, ImageIcon> IMAGE_CACHE;

    /**
     * <pre>
     * Generates an icon that can be used elsewere in the application.
     * </pre>
     * @param imgLoc The URL of the image
     * @param width The desired icon width
     * @param height The desired icon height
     * @return The generated icon
     */
    public static ImageIcon generateIcon(String imgLoc, int width, int height) {
        if(IMAGE_CACHE == null)  IMAGE_CACHE = new HashMap<String, ImageIcon>();
        if(IMAGE_CACHE.containsKey(imgLoc)) return IMAGE_CACHE.get(imgLoc);
        try {
            URL url = new URL(imgLoc);
            ImageIcon icon = new ImageIcon(new ImageIcon(url).getImage().getScaledInstance(width, height, Image.SCALE_DEFAULT));
            IMAGE_CACHE.put(imgLoc, icon);
            return icon;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String currentUserType;
    private JFrame window = new JFrame();
    private MyModel backend;
    private String currentUserID;
    private String adminID;

    /**
     * <pre>
     * Creates a new instance of ShopController.
     * Make sure to call the "init" method after this!
     * </pre>
     * @param b The Model with all of the back-end links that the store is to use
     */
    public ShopController(MyModel b) {
        this.backend = b;
    }

    /**
     * <pre>
     * Sets the store's current view after setting the view's controller and initializing it.
     * </pre>
     * @param view The view to set
     */
    public void setView(View view) {
        view.setController(this);
        view.initialize();
        window.setContentPane(view);
        window.revalidate();
    }

    /**
     * @return The JFrame that holds the store.
     */
    public JFrame getWindow() {
        return window;
    }

    /**
     * @return The Model instance controlling the store.
     */
    public MyModel getBackend() {
        return this.backend;
    }

    /**
     * <pre>
     * Initialize and show the store window.
     * Also displays the LoginView.
     * </pre>
     */
    public void init() {
        //window.setResizable(false);
        this.adminID = null;
        this.currentUserID = null;
        window.setTitle("Monash Online Shopping System");
        window.setSize(1000, 800);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);
        window.setVisible(true);
        window.addWindowListener(new WindowAdapter() {
          public void windowClosing(WindowEvent e) {
            getBackend().save();
            System.exit(0);
          }
        });
        this.setView(new LoginView());
    }

    public void logout() {
      init();
    }

    /**
     * <pre>
     * Shows a popup message.
     * </pre>
     * @param message The text in the popup window.
     */
    public void showPopup(String message) {
        JOptionPane.showMessageDialog(window, message);
    }

    public Customer getCurrentCustomerDetails() {
        return getBackend().getUserInfo(currentUserID);
    }
    public ArrayList<Hotel> searchHotel(Date s, Date t, String name) {
        return backend.getHotels(s, t, name, false);
    }
    public ArrayList<Cabin> searchCabin(Date s, Date t, String name) {
        return backend.getCabins(s, t, name, false);
    }
    public ArrayList<Apartment> searchApartments(Date s, Date t, String name) {
        return backend.getApartments(s, t, name, false);
    }
    public ArrayList<Hotel> adminSearchHotel(Date s, Date t, String name) {
        return backend.getHotels(s, t, name, true);
    }
    public ArrayList<Cabin> adminSearchCabin(Date s, Date t, String name) {
        return backend.getCabins(s, t, name, true);
    }
    public ArrayList<Apartment> adminSearchApartment(Date s, Date t, String name) {
        return backend.getApartments(s, t, name, true);
    }

    public void signup(String username, String pass, String confPass) {

        // Ensuring length
        if(username.length() < 3) {
            showPopup("Your user ID must be at least 3 chars long!");
            return;
        } else if(pass.length() < 5) {
            showPopup("Your password must be at least 5 chars long!");
            return;
        } else if(!pass.equals(confPass)) {
            showPopup("The passwords do not match");
            return;
        }

        boolean success = getBackend().signup(username, pass);

        if(!success) {
            showPopup("Signup failed, that userID may already be in use!");
        } else {
            showPopup("Your account has been created, please edit your details by clicking 'My account' in the top right.");
            attemptLogin(username, pass, "customer");
        }
    }

    public void attemptLogin(String username, String password, String loginType) {
      if (loginType.equals("customer")) {
        if(backend.loginCustomer(username, password)) {
          currentUserID = username;
          window.setTitle("Welcome to Monash Online Shopping System");
          showProductList();
        } else {
          showPopup("Login failed! Please ensure that your user ID and password are correct.");
        }
      } else {
        if (backend.loginAdmin(username, password)) {
          adminID = username;
          window.setTitle("Monash Online Shopping System (admin)");
          showManageList();
        } else {
          showPopup("Login failed! Please ensure that your user ID and password are correct.");
        }
      }
      currentUserType = loginType;
    }

    public void updateUserDetails(Customer c) {
        if(this.currentUserID != null) {
            boolean success = getBackend().setUserInfo(this.currentUserID, c);
            if(!success) {
                showPopup("There was an error saving your information! Please try again later.");
            }
        } else {
            System.err.println("Can't update user info, no one is signed in!");
        }
    }


    /*
     * PRODUCTS
     * ------------------------------------------------
     */

    /**
     * <pre>
     * Shows the checkout dialog.
     * </pre>
     */
    public void showCheckout() {
        ConfirmDialog.display(this);
    }

    /**
     * @return The current user's cart.
     */
    public Customer getCustomer() {
      return getBackend().getUserInfo(currentUserID);
    }
    public Cart getCart() {
        return getCustomer().cart;
    }

    public void addToCart(Accommodation p, int quantity, Date s, Date t, String style) {
        ArrayList<Room> rooms = p.searchRooms(s, t, style);
        for (int i=0; i<quantity && i<rooms.size(); i++) {
          Room r = rooms.get(i);
          backend.bookRoom(p.getAcID(), r.getRoomID(), s, t);
          getCustomer().cart.add(getBackend(), p, r.getRoomID(), s, t);
        }
    }

    /**
     * <pre>
     * Shows the cart view.
     * </pre>
     */
    public void showCartView() {
        setView(new CartView());
    }

    /**
     * <pre>
     * See "Model.getPrice(Cart)" for more information.
     * </pre>
     * @return The total price of all item in the cart
     */
    public float getTotalCartPrice() {
        return getBackend().getPrice(getCustomer().cart);
    }

    /**
     * <pre>
     * Shows the product list view.
     * </pre>
     */
    public void showProductList() {
      setView(new ProductListView());
    }

    public void showManageList() {
      setView(new AdminManageView());
    }

    public void attemptTransaction() {
        Customer c = getBackend().getUserInfo(currentUserID);
        String prefix = "Order failed! ";
        if(c.name.trim().equals("")) {
            showPopup(prefix + "You have not entered your full name!");
            return;
        } else if(c.address.trim().equals("")) {
            showPopup(prefix + "You have not entered your home address!");
            return;
        } else if(c.phoneNumber.trim().equals("")) {
            showPopup(prefix + "You have not entered your phone number!");
            return;
        } else if(c.cardNumber.trim().equals("")) {
            showPopup(prefix + "You have not entered your card number!");
            return;
        }

        boolean success = getBackend().processOrder(currentUserID, getCustomer().cart);

        if(!success) {
            showPopup("Sorry, your order could not be placed! Please ensure that all of your information is correct.");
        } else {
            showPopup("Your order has been placed successfully! Have a nice day!");
            this.getCustomer().cart.clear();
            this.showCartView();
        }
    }

    public String attemptCreateAccommodation(String type, String name, String addr,
        String sPrice, String stPrice, String fPrice) {
      if (sPrice.matches("\\d+(\\.\\d+)?") &&
          stPrice.matches("\\d+(\\.\\d+)?") &&
          fPrice.matches("\\d+(\\.\\d+)?")) {
        float p1 = Float.parseFloat(sPrice);
        float p2 = Float.parseFloat(stPrice);
        float p3 = Float.parseFloat(fPrice);
        if (type.equals("Hotel")) {
          String id = getBackend().addHotel(name, addr, p1, p2, p3);
          return id;
        } else if (type.equals("Cabin")) {
          String id = getBackend().addCabin(name, addr, p1, p2, p3);
          return id;
        } else {
          String id = getBackend().addApartment(name, addr, p1, p2, p3);
          return id;
        }
      } else {
        showPopup("Price must be correct float fomat.");
        return null;
      }
    }
}
