import java.io.Serializable;
import java.util.*;

public class Transaction implements Serializable {
  public Date startDate;
  public Date endDate;
  public float amount;

  public Transaction(Date s, Date t, float amount) {
    this.startDate = s;
    this.endDate = t;
    this.amount = amount;
  }
}
