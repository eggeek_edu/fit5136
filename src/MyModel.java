import java.io.FileOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.util.*;

/**
 * This is an revise version of Model, store all the instance and implement ID generator
 */

public class MyModel implements Model {

    public HashMap<String, Hotel> hotels = new HashMap<>();
    public HashMap<String, Cabin> cabins = new HashMap<>();
    public HashMap<String, Apartment> apartments = new HashMap<>();
    public HashMap<String, Order> orders = new HashMap<>();
    public HashMap<String, CartItem> items = new HashMap<>();
    public HashMap<String, PaymentRecord> payments = new HashMap<>();
    public HashMap<String, RefundRecord> refunds = new HashMap<>();
    // username, password for customer
    public HashMap<String, String> passwords = new HashMap<>();
    public HashMap<String, Customer> details = new HashMap<>();
    public String dataDir = "data/";

    // username, password for admin
    public HashMap<String, String> adminPwds = new HashMap<>();
    public HashMap<String, Admin> admins = new HashMap<>();

    // ID cursor
    private int accommodationIDGen = 0;
    private int itemIDGen = 0;
    private int paymentIDGen = 0;
    private int refundIDGen = 0;
    private int orderIDGen = 0;

    public MyModel() {
      loadData();
      loadAccount();
    }

    public MyModel(String dataDir) {
      dataDir = dataDir.replaceAll("/$", "") + "/";
      this.dataDir = dataDir;
      loadData();
      loadAccount();
    }

    /**
     * Save an instance to file
     * @param fn the file name
     * @param obj the instance
     */
    public <T> void saveObj (String fn, T obj) {
      System.out.println("Saving to file: " + fn);
      try {
        ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fn));
        os.writeObject(obj);
        os.close();
        System.out.println("Save success.");
      } catch (FileNotFoundException e){
        e.printStackTrace();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    /**
     * Read an instance from file
     * @return the instance read from file
     */
    public <T> T readObj(String fn, T obj) {
      try {
        ObjectInputStream in = new ObjectInputStream(new FileInputStream(fn));
        obj = (T)in.readObject();
        in.close();
        return obj;
      } catch(FileNotFoundException e){
        File f = new File(fn);
        if (f.getParentFile() != null)
          f.getParentFile().mkdirs();
        try {
          f.createNewFile();
        } catch(IOException ei){
          ei.printStackTrace();
        }
      } catch(IOException e) {
        //System.out.println("Finish read file: " + fn);
      } catch(ClassNotFoundException e) {
        e.printStackTrace();
      }
      return null;
    }

    private void loadAccount() {
      Admin admin = new Admin("admin", "admin");
      adminPwds.put(admin.getUsername(), admin.getPassword());
      admins.put(admin.getUsername(), admin);

      Customer me = new Customer("szha414", "s", "shizhe zhao", "caulfield", "123", "456");
      if (!passwords.containsKey(me.getUsername())) {
        passwords.put(me.getUsername(), me.getPassword());
        details.put(me.getUsername(), me);
      }
    }

    private void loadData() {
      this.passwords = readObj(dataDir + "passwords", passwords);
      this.details = readObj(dataDir + "details", details);
      this.hotels = readObj(dataDir + "hotels", hotels);
      this.cabins = readObj(dataDir + "cabins", cabins);
      this.apartments = readObj(dataDir + "apartments", apartments);
      this.orders = readObj(dataDir + "orders", orders);
      this.items = readObj(dataDir + "items", items);
      this.payments = readObj(dataDir + "payments", payments);
      this.refunds = readObj(dataDir + "refunds", refunds);

      if (passwords == null) passwords = new HashMap<>();
      if (details == null) details = new HashMap<>();
      if (hotels == null) hotels = new HashMap<>();
      if (cabins == null) cabins = new HashMap<>();
      if (apartments == null) apartments = new HashMap<>();
      if (orders == null) orders = new HashMap<>();
      if (items == null) items = new HashMap<>();
      if (payments == null) payments = new HashMap<>();
      if (refunds == null) refunds = new HashMap<>();
      itemIDGen = items.size();
      orderIDGen = orders.size();
      paymentIDGen = payments.size();
      refundIDGen = refunds.size();
      accommodationIDGen = hotels.size() + cabins.size() + apartments.size();
    }

    public void save() {
      saveObj(dataDir + "hotels", hotels);
      saveObj(dataDir + "cabins", cabins);
      saveObj(dataDir + "apartments", apartments);
      saveObj(dataDir + "orders", orders);
      saveObj(dataDir + "items", items);
      saveObj(dataDir + "payments", payments);
      saveObj(dataDir + "refunds", refunds);
      saveObj(dataDir + "apartments", apartments);
      saveObj(dataDir + "hotels", hotels);
      saveObj(dataDir + "passwords", passwords);
      saveObj(dataDir + "details", details);
      saveObj(dataDir + "itemIDGen", itemIDGen);
      saveObj(dataDir + "orderIDGen",orderIDGen);
      saveObj(dataDir + "refundIDGen", refundIDGen);
      saveObj(dataDir + "paymentIDGen", paymentIDGen);
      saveObj(dataDir + "accommodationIDGen", accommodationIDGen);
    }

    public <T extends Accommodation> void setPrices(T accommodation, float singlePrice, float standardPrice, float familyPrice) {
      accommodation.setSinglePrice(singlePrice);
      accommodation.setStandardPrice(standardPrice);
      accommodation.setFamilyPrice(familyPrice);
    }

    /**
     * Create a hotel
     * @param acName hotel name
     * @param address hotel address
     * @param singlePrice price of single room ($/day)
     * @param standardPrice price of standard room ($/day)
     * @param familyPrice price of family room ($/day)
     * @return hotel ID
     */
    public String addHotel(String acName, String address, float singlePrice, float standardPrice, float familyPrice) {
      Hotel h = new Hotel(Integer.toString(nextAccommodationID()), acName, address);
      hotels.put(h.getAcID(), h);
      setPrices(h, singlePrice, standardPrice, familyPrice);
      return h.getAcID();
    }

    /**
     * Create a cabin by default price
     * @param acName cabin name
     * @param address cabin address
     * @param singlePrice price of single room ($/day)
     * @param standardPrice price of standard room ($/day)
     * @param familyPrice price of family room ($/day)
     * @return cabin ID
     */
    public String addCabin(String acName, String address, float singlePrice, float standardPrice, float familyPrice) {
      Cabin c = new Cabin(Integer.toString(nextAccommodationID()), acName, address);
      cabins.put(c.getAcID(), c);
      setPrices(c, singlePrice, standardPrice, familyPrice);
      return c.getAcID();
    }

    /**
     * Create a apartment by default price
     * @param acName apartment name
     * @param address apartment addr
     * @param singlePrice price of single room ($/day)
     * @param standardPrice price of standard room ($/day)
     * @param familyPrice price of family room ($/day)ess
     * @return apartment ID
     */
    public String addApartment(String acName, String address, float singlePrice, float standardPrice, float familyPrice) {
      Apartment a = new Apartment(Integer.toString(nextAccommodationID()), acName, address);
      apartments.put(a.getAcID(), a);
      setPrices(a, singlePrice, standardPrice, familyPrice);
      return a.getAcID();
    }

    /**
     * Search accommodation
     * @param s start date
     * @param t end date
     * @param name the accommodation name
     * @param ac a HashMap<String, T> store accommodation, T is a subclass of Accommodation
     * @param booked ture or false, represent the status of returned result
     * @return ArrayList<T> accommodations match the search condition
     */
    public <T extends Accommodation> ArrayList<T> getAccommodations(Date s, Date t, String name, HashMap<String, T> ac, boolean booked) {
      ArrayList<T> res = new ArrayList<>();
      if (!booked) {
        for (T i: ac.values()) if (i.matchName(name) && i.availableNum(s, t) > 0) res.add(i);
      } else {
        for (T i: ac.values()) if (i.matchName(name) && i.availableNum(s, t) < i.totalNum()) res.add(i);
      }
      return res;
    }
    /**
     * Only Search Hotel, will call getAccommodations
     * @see #getAccommodations(Date s, Date t, String name, HashMap<String, T> ac, boolean booked)
     */
    public ArrayList<Hotel> getHotels(Date s, Date t, String name, boolean booked) {
      return getAccommodations(s, t, name, hotels, booked);
    }
    /**
     * Only Search Cabin
     * @see #getAccommodations(Date s, Date t, String name, HashMap<String, T> ac, boolean booked)
     */
    public ArrayList<Cabin> getCabins(Date s, Date t, String name, boolean booked) {
      return getAccommodations(s, t, name, cabins, booked);
    }
    /**
     * Only Search Apartment
     * @see #getAccommodations(Date s, Date t, String name, HashMap<String, T> ac, boolean booked)
     */
    public ArrayList<Apartment> getApartments(Date s, Date t, String name, boolean booked) {
      return getAccommodations(s, t, name, apartments, booked);
    }

    public boolean loginCustomer(String username, String password) {
        if(passwords.containsKey(username)) {
            return passwords.get(username).equals(password);
        }
        return false;
    }

    public boolean loginAdmin(String username, String password) {
      if (adminPwds.containsKey(username)) {
        return adminPwds.get(username).equals(password);
      }
      return false;
    }

    public boolean signup(String username, String password) {
        if(passwords.containsKey(username)) return false;
        passwords.put(username, password);
        details.put(username, new Customer(username, password, "", "", "", ""));
        return true;
    }

    public Customer getUserInfo(String username) {
        return details.get(username);
    }

    public boolean setUserInfo(String username, Customer info) {
        details.put(username, info);
        return true;
    }

    public float getPrice(Cart cart) {
        float total = 0;
        for(CartItem item : cart.getList()) total += item.price;
        return total;
    }

    /**
     * Create order from all items in cart
     * @param currentUserID
     * @param cart
     * @return boolean, true represent create order success, false represent create order fail
     */
    public boolean processOrder(String currentUserID, Cart cart) {
      String orderID = Integer.toString(nextOrderID());
      ArrayList<String> itemIDs = new ArrayList<>();
      for (CartItem i: cart.getList()) itemIDs.add(i.itemID);
      Order order = new Order(orderID, currentUserID, itemIDs, getPrice(cart));
      orders.put(order.orderID, order);
      PaymentRecord p = order.setFinish(Integer.toString(nextPaymentID()));
      payments.put(p.paymentID, p);
      details.get(currentUserID).orderIDs.add(orderID);
      details.get(currentUserID).paymentIDs.add(p.paymentID);
      return true;
    }

    public void bookRoom(String acID, String roomID, Date s, Date t) {
      if (hotels.containsKey(acID)) {
        Hotel h = hotels.get(acID);
        h.bookRoom(roomID, s, t);
      } else if (cabins.containsKey(acID)) {
        Cabin c = cabins.get(acID);
        c.bookRoom(roomID, s, t);
      } else {
        Apartment a = apartments.get(acID);
        a.bookRoom(roomID, s, t);
      }
    }

    public void cancelBookRoom(String acID, String roomID, Date s, Date t) {
      if (hotels.containsKey(acID)) {
        Hotel h = hotels.get(acID);
        h.cancelBookRoom(roomID, s, t);
      } else if (cabins.containsKey(acID)) {
        Cabin c = cabins.get(acID);
        c.cancelBookRoom(roomID, s, t);
      } else {
        Apartment a = apartments.get(acID);
        a.cancelBookRoom(roomID, s, t);
      }
    }

    public int nextAccommodationID() { return accommodationIDGen++; }
    public int nextItemID() { return itemIDGen++; }
    public int nextOrderID() { return orderIDGen++; }
    public int nextPaymentID() { return paymentIDGen++; }
    public int nextRefundID() { return refundIDGen++; }
}
