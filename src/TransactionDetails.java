import java.awt.*;
import java.awt.event.*;
import javax.swing.table.*;
import javax.swing.*;
import java.util.*;


public class TransactionDetails extends JDialog {
  private static final long serialVersionUID = 1L;
  private final JPanel contentPanel = new JPanel();
  private JLabel historyLB = new JLabel();
  private JPanel content = new JPanel();
  private ShopController c;
  private JScrollPane scr;
  private DefaultTableModel dataModel;

  public static void display(ShopController c) {
    TransactionDetails dialog = new TransactionDetails(c);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setLocationRelativeTo(c.getWindow());
    dialog.setVisible(true);
  }

  private void setPayments() {
    historyLB.setText(c.getCustomer().toPaymentHtml(c.getBackend()));
  }

  private void setRefunds() {
    historyLB.setText(c.getCustomer().toRefundHtml(c.getBackend()));
  }

  private void addTopPanel() {
    JPanel p = new JPanel(new FlowLayout());
    JButton payments = new JButton("Payments");
    JButton refunds = new JButton("Refunds");
    p.add(payments, BorderLayout.EAST);
    p.add(refunds, BorderLayout.EAST);

    payments.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setPayments();
      }
    });

    refunds.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        setRefunds();
      }
    });
    add(p, BorderLayout.NORTH);
  }

  private void addButtonPanel() {
    JPanel p = new JPanel(new FlowLayout());
    JButton closeButton = new JButton("Close");
    p.add(closeButton, BorderLayout.EAST);
    closeButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });
    add(p, BorderLayout.SOUTH);
  }

  private void addCentralPanel() {
    scr = new JScrollPane(content);
    scr.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scr.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    content.setLayout(new BorderLayout());
    content.add(historyLB, BorderLayout.CENTER);
    add(scr, BorderLayout.CENTER);
  }

  public TransactionDetails(ShopController c) {
    this.c = c;
    setBounds(100, 100, 550, 500);
    getContentPane().setLayout(new BorderLayout());
    setPayments();
    addTopPanel();
    addCentralPanel();
    addButtonPanel();
  }
}
