import java.io.Serializable;
import java.util.*;

public class CartItem implements Serializable {

    public String itemID;
    public Accommodation accommodation;
    public String acID;
    public String roomID;
    public Date s;
    public Date t;
    public float price;

    public CartItem(String itemID, Accommodation p, String roomID, Date s, Date t) {
      this.itemID = itemID;
      this.accommodation = p;
      this.acID = p.getAcID();
      this.roomID = roomID;
      this.s = s;
      this.t = t;
      this.price = p.getRoomByID(roomID).getPrice();
      Room r = p.getRoomByID(roomID);
      //System.out.printf("create cartItem, ID: %s, price: %s, style: %s\n", r.getRoomID(), price, p.getRoomStyle(roomID));
    }
}
