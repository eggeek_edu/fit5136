import java.io.Serializable;
import java.util.*;
import javax.swing.ImageIcon;

class RoomSet implements Serializable {
  public String style;
  public HashMap<String, Room> rooms;
  public int bathroomNum;
  public int washroomNum;
  public int bedNum;
  public float price;

  public RoomSet(String style, int bathroomNum, int washroomNum, int bedNum, float price) {
    this.bathroomNum = bathroomNum;
    this.washroomNum = washroomNum;
    this.bedNum = bedNum;
    this.price = price;
    this.style = style;
    rooms = new HashMap<>();
  }

  public void addRoom(Room r) {
    //System.out.printf("add room, ID: %s, price: %s, style: %s\n", r.getRoomID(), price, style);
    r.setPrice(price);
    rooms.put(r.getRoomID(), r);
  }

  public void setPrice(float price) {this.price = price;}

  public ArrayList<Room> searchRooms(Date s, Date t) {
    ArrayList<Room> res = new ArrayList<Room>();
    for (Room i: rooms.values()) if (i.isAvailable(s, t)) res.add(i);
    return res;
  }
  public int availableNum(Date s, Date t) { return searchRooms(s, t).size(); }
  public int totalNum() { return rooms.size(); }
  public ArrayList<Room> getAllRooms() {
    ArrayList<Room> res = new ArrayList<Room>();
    for (Room i: rooms.values()) res.add(i);
    return res;
  }
  public String toHtml(Date s, Date t) {
    String out = "<html>";
    out += String.format("Each has %d bathroom(s)", bathroomNum) + "<br>";
    out += String.format("Each has %d washroom(s)", washroomNum) + "<br>";
    out += String.format("Each has %d bedroom(s)", bedNum) + "<br>";
    out += String.format("Price: %.2f$", price) + "<br>";
    out += String.format("Total Rooms: %d", rooms.size()) + "<br>";
    out += String.format("Available: %d",availableNum(s, t));
    out += "</html>";
    return out;
  }
}

public class Accommodation implements Serializable {
  public RoomSet single;
  public RoomSet standard;
  public RoomSet family;
  private Date createDate;
  private String acID;
  private String acName;
  private HashMap<String, Room> rooms;
  private String address;
  private ImageIcon image;
  private int roomIDGen;

  public Accommodation(String acID, String acName, String address) {
    single = new RoomSet("single", 1, 1, 1, 100);
    standard = new RoomSet("standard", 1, 1, 2, 200);
    family = new RoomSet("family", 2, 2, 3, 300);
    this.acID = acID;
    this.acName = acName;
    this.address = address;
    this.rooms = new HashMap<String, Room>();
    this.createDate = new Date();
    roomIDGen = 0;
  }

  public String getAcID() { return acID; }
  public String getAcName() { return acName; }
  public String getAddress() { return address; }

  public void addRooms(String style, int num) {
    for (int i=0; i<num; i++) {
      Room r = new Room(acID, acName);
      r.setRoomID(Integer.toString(nextRoomID()));
      if (style.equals("single")) single.addRoom(r);
      else if (style.equals("standard")) standard.addRoom(r);
      else family.addRoom(r);
    }
  }

  public Room getRoomByID(String roomID) {
    if (single.rooms.containsKey(roomID)) return single.rooms.get(roomID);
    else if (standard.rooms.containsKey(roomID)) return standard.rooms.get(roomID);
    else if (family.rooms.containsKey(roomID)) return family.rooms.get(roomID);
    else return null;
  }

  public String getRoomStyle(String roomID) {
    if (single.rooms.containsKey(roomID)) return "single";
    else if (standard.rooms.containsKey(roomID)) return "standard";
    else return "family";
  }

  public void bookRoom(String roomID, Date s, Date t) { getRoomByID(roomID).book(s, t); }
  public void cancelBookRoom(String roomID, Date s, Date t) { getRoomByID(roomID).unbook(s, t); }
  public ImageIcon getImage(){
      if(this.image == null) return ShopController.NO_IMAGE_ICON;
      else return this.image;
  }

  public ArrayList<Room> searchRooms(Date s, Date t, String style) {
    if (style.equals("single")) return single.searchRooms(s, t);
    else if (style.equals("standard")) return standard.searchRooms(s, t);
    else return family.searchRooms(s, t);
  }

  @Override
  public String toString() {
    String s = String.format("acID:%s\nacName:%s\naddr:%s\nrooms:%d\n",
        acID, acName, address, rooms.size());
    return s;
  }

  public boolean matchName(String name) {
    return acName.contains(name);
  }

  public int availableNum(Date s, Date t) {
    return single.availableNum(s, t) + standard.availableNum(s, t) + family.availableNum(s, t);
  }

  public int totalNum() {
    return single.totalNum() + standard.totalNum() + family.totalNum();
  }

  public int styleAvailableNum(Date s, Date t, String style) {
    if (style.equals("single")) return single.availableNum(s, t);
    else if (style.equals("standard")) return standard.availableNum(s, t);
    else return family.availableNum(s, t);
  }

  public int styleBookedNum(Date s, Date t, String style) {
    if (style.equals("single")) return single.rooms.size() - single.availableNum(s, t);
    else if (style.equals("standard")) return standard.rooms.size() - standard.availableNum(s, t);
    else return family.rooms.size() - family.availableNum(s, t);
  }

  public String toHtml(Date s, Date t, String style) {
    String res = "";
    if (style.equals("single")) res = single.toHtml(s, t);
    else if (style.equals("standard")) res = standard.toHtml(s, t);
    else if (style.equals("family")) res = family.toHtml(s, t);
    return res;
  }

  public int nextRoomID() { return roomIDGen++; }

  /**
   * Generate report of this accommodation
   * @param b backend, the data collection
   * @param s start date
   * @param t end date
   * @param style the style of room (single, standard or family)
   * @return customer and the number of room they booked
   */
  public HashMap<Customer, Integer> genReport(MyModel b, Date s, Date t, String style) {
    HashMap<Customer, Integer> res = new HashMap<>();
    for (Order i: b.orders.values()) {
      for (String itemID: i.itemIDs) {
        CartItem item = b.items.get(itemID);
        //System.out.printf("itemID: %s, acID: %s, roomID: %s\n", item.itemID, item.acID, item.roomID);
        if (item.accommodation.getAcID().equals(this.acID) && getRoomStyle(item.roomID).equals(style)) {
          Customer c = b.details.get(i.customerID);
          if (res.containsKey(c)) res.put(c, res.get(c) + 1);
          else res.put(c, 1);
        }
      }
    }
    return res;
  }

  public void setSinglePrice(float price) { single.setPrice(price); }
  public void setStandardPrice(float price) { standard.setPrice(price); }
  public void setFamilyPrice(float price) { family.setPrice(price); }

  /**
   * Format report data to HTML
   * @see #genReport(MyModel b, Date s, Date t, String style)
   */
  public String toReportHTML(MyModel b, Date s, Date t, String style) {
    String header = "<html><style>th, td {border: 1px solid black;}</style>" +
      "<table style=\"width:100%\">";
    String tail = "</table></html>";
    ArrayList<String> table = new ArrayList<>();
    table.add(header);
    table.add("<tr>" +
      "<th>ID</th>" + "<th>Name</th>" + "<th>Phone</th>" +
      "<th>Card Num</th>" + "<th>Booked Num</th>" +
      "</tr>");
    HashMap<Customer, Integer> res = genReport(b, s, t, style);
    for (Customer i: res.keySet()) {
      table.add("<tr>");
      table.add(String.format("<td>%s</td>", i.getUsername()));
      table.add(String.format("<td>%s</td>", i.name));
      table.add(String.format("<td>%s</td>", i.phoneNumber));
      table.add(String.format("<td>%s</td>", i.cardNumber));
      table.add(String.format("<td>%d</td>", res.get(i)));
      table.add("</tr>");
    }
    table.add(tail);
    return String.join("\n", table);
  }
}
