import java.io.Serializable;
import java.util.Date;

class DateRange implements Serializable {
  private Date startDate;
  private Date endDate;

  public DateRange(Date s, Date t) {
    this.startDate = s;
    this.endDate = t;
  }

  public Date getStart() { return startDate; }
  public Date getEnd() { return endDate; }

  public boolean isCollide(DateRange r) {
    if (r.getEnd().before(startDate) || r.getStart().after(endDate)) return false;
    return true;
  }
  @Override
  public int hashCode() {
    return toString().hashCode();
  }
  @Override
  public boolean equals(Object b) {
    return this.hashCode() == b.hashCode();
  }
  @Override
  public String toString() {
    return startDate.toString() + "|" + endDate.toString();
  }
}
