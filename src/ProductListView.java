import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.*;

public class ProductListView extends View {

  private static final long serialVersionUID = 1L;

  private JPanel scrollPanel;

  public <T extends Accommodation> void displaySearch(ArrayList<T> list, Date s, Date t) {
    scrollPanel.removeAll();
    for(Accommodation p : list) {
      scrollPanel.add(new AccommodationThumbnail(getController(), p, s, t));
    }
    scrollPanel.revalidate();
    scrollPanel.repaint();
  }

  public void createSearchButton(JPanel searcher) {
    // search parameter
    searcher.add(new JLabel("start date:"));
    JSpinner start = new JSpinner();
    start.setModel(new SpinnerDateModel(new Date(), null, null, 1));
    start.setEditor(new JSpinner.DateEditor(start, "yyyy-MM-dd"));
    searcher.add(start);

    searcher.add(new JLabel("days:"));
    JSpinner days = new JSpinner();
    days.setModel(new SpinnerNumberModel(1, 0, 100000, 1));
    searcher.add(days);

    searcher.add(new JLabel("type:"));
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    model.addElement("hotel");
    model.addElement("cabin");
    model.addElement("apartment");
    JComboBox comboBox = new JComboBox(model);
    searcher.add(comboBox);

    searcher.add(new JLabel("name:"));
    JTextField searchName = new JTextField();
    searchName.setColumns(10);
    searcher.add(searchName);

    JButton searchButton = new JButton("Search");
    searcher.add(searchButton);
    searchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String type = comboBox.getSelectedItem().toString();
        int dur = (int)days.getModel().getValue();
        Date s = (Date)start.getModel().getValue();
        Date t = new Date(s.getTime() + (1000 * 3600 * 24) * dur);
        String name = searchName.getText();
        if (type.equals("hotel")) {
          ArrayList<Hotel> res = getController().searchHotel(s, t, name);
          displaySearch(res, s, t);
        }
        if (type.equals("cabin")) {
          ArrayList<Cabin> res = getController().searchCabin(s, t, name);
          displaySearch(res,s ,t);
        }
        if (type.equals("apartment")) {
          ArrayList<Apartment> res = getController().searchApartments(s, t, name);
          displaySearch(res,s, t);
        }
      }
    });
  }

  public void createAccountButton(JPanel panel) {
    JButton myInfoButton = new JButton("My account");
    panel.add(myInfoButton, BorderLayout.EAST);

    myInfoButton.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        UserDetails.display(getController());
      }
    });
  }

  public void createMyTransaction(JPanel panel) {
    JButton myTransaction = new JButton("My transaction");
    panel.add(myTransaction, BorderLayout.EAST);

    myTransaction.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        TransactionDetails.display(getController());
      }
    });
  }

  public void setUpLayOut(JPanel panel, String blayout) {
    FlowLayout flowLayout = (FlowLayout) panel.getLayout();
    flowLayout.setAlignment(FlowLayout.RIGHT);
    add(panel, blayout);
  }

  public void creatViewCartButton(JPanel panel) {
    JButton cartButton = new JButton("View cart");
    cartButton.addActionListener(new ActionListener(){
      public void actionPerformed(ActionEvent e) {
        getController().showCartView();
      }
    });
    panel.add(cartButton, BorderLayout.EAST);
  }

  public void creatLogoutButton(JPanel panel) {
    JButton logoutButton = new JButton("Log out");
    logoutButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        getController().logout();
      }
    });
    panel.add(logoutButton, BorderLayout.WEST);
  }

  public void createBottomPanel(JPanel bottom) {
    bottom.setLayout(new BorderLayout());
    creatLogoutButton(bottom);
    JPanel brPanel = new JPanel(new FlowLayout());
    createMyTransaction(brPanel);
    createAccountButton(brPanel);
    creatViewCartButton(brPanel);
    bottom.add(brPanel, BorderLayout.EAST);
  }

  public ProductListView() {
    setLayout(new BorderLayout(0, 0));

    JPanel bottom = new JPanel();
    createBottomPanel(bottom);
    add(bottom, BorderLayout.SOUTH);

    JPanel searcher = new JPanel();
    setUpLayOut(searcher, BorderLayout.NORTH);
    createSearchButton(searcher);

    scrollPanel = new JPanel();
    JScrollPane scroll = new JScrollPane(scrollPanel);
    scrollPanel.setLayout(new GridLayout(0, 3, 0, 0));
    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    add(scroll, BorderLayout.CENTER);
  }

  public void initialize() {
  }
}
