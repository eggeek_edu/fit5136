import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.EmptyBorder;

public class CreateAccommodation extends JDialog {
  private static final long serialVersionUID = 1L;
  private ShopController c;

  private JPanel contentPanel = new JPanel();
  private JPanel extraPanel = new JPanel();
  private JPanel actionPanel = new JPanel();
  // basic
  private JTextField name = new JTextField();
  private JTextField address = new JTextField();
  private JTextField singlePrice = new JTextField();
  private JTextField standardPrice = new JTextField();
  private JTextField familyPrice = new JTextField();
  private JSpinner singleRoom = new JSpinner();
  private JSpinner standardRoom = new JSpinner();
  private JSpinner familyRoom = new JSpinner();
  private JComboBox typeBox = new JComboBox();
  // hotel
  private JSpinner starLevel = new JSpinner();
  // cabin
  private JTextField landscape = new JTextField();
  private JSpinner gardenNum = new JSpinner();
  // apartment
  private JSpinner kitchenNum = new JSpinner();


  public static void display(ShopController c) {
    CreateAccommodation dialog = new CreateAccommodation(c);
    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    dialog.setLocationRelativeTo(c.getWindow());
    dialog.setVisible(true);
  }

  public static void addOptions(JPanel panel) {

  }

  private GridBagConstraints genConstraint(int x, int y) {
    GridBagConstraints res = new GridBagConstraints();
    res.insets = new Insets(0, 0, 20, 20);
    res.gridx = x;
    res.gridy = y;
    return res;
  }

  private void drawExtraPanel(String type) {
    extraPanel.removeAll();
    extraPanel.setLayout(new GridBagLayout());
    extraPanel.add(new JLabel("Extra information"), genConstraint(1, 0));
    if (type.equals("Hotel")) {
      starLevel.setModel(new SpinnerNumberModel(1, 1, 5, 1));
      extraPanel.add(new JLabel("Star Level:"), genConstraint(1, 1));
      extraPanel.add(starLevel, genConstraint(2, 1));
    } else if (type.equals("Cabin")) {
      landscape.setColumns(10);
      landscape.setText("Mountain");
      gardenNum.setModel(new SpinnerNumberModel(1, 1, 100, 1));
      extraPanel.add(new JLabel("Landscape:"), genConstraint(1, 1));
      extraPanel.add(landscape, genConstraint(2, 1));

      extraPanel.add(new JLabel("Garden Num:"), genConstraint(1, 2));
      extraPanel.add(gardenNum, genConstraint(2, 2));
    } else {
      kitchenNum.setModel(new SpinnerNumberModel(1, 1, 10, 1));
      extraPanel.add(new JLabel("Kitchen Num:"), genConstraint(1, 1));
      extraPanel.add(kitchenNum, genConstraint(2, 1));
    }
    extraPanel.revalidate();
    extraPanel.repaint();
  }

  private void drawBasicPanel() {
    contentPanel.setLayout(new GridBagLayout());
    int y = 1;

    contentPanel.add(new JLabel("Basic information"), genConstraint(1, y++));

    JLabel typeLabel = new JLabel("Type:");
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    model.addElement("Hotel");
    model.addElement("Cabin");
    model.addElement("Apartment");
    typeBox.setModel(model);
    contentPanel.add(typeLabel, genConstraint(1, y));
    contentPanel.add(typeBox, genConstraint(2, y));
    typeBox.addActionListener(new ActionListener() {
      public void  actionPerformed(ActionEvent e) {
        drawExtraPanel(typeBox.getSelectedItem().toString());
      }
    });
    y++;

    JLabel nameLabel = new JLabel("Name:");
    name.setColumns(10);
    contentPanel.add(nameLabel, genConstraint(1, y));
    contentPanel.add(name, genConstraint(2, y));
    y++;

    JLabel addrLabel = new JLabel("Address:");
    address.setColumns(10);
    contentPanel.add(addrLabel, genConstraint(1, y));
    contentPanel.add(address, genConstraint(2, y));
    y++;

    // single room
    JLabel singlePriceLabel = new JLabel("Single Room Price($/day):");
    singlePrice.setColumns(5);
    contentPanel.add(singlePriceLabel, genConstraint(1, y));
    contentPanel.add(singlePrice, genConstraint(2, y));
    y++;
    JLabel singleLabel = new JLabel("Single room num:");
    singleRoom.setModel(new SpinnerNumberModel(1, 0, 100, 1));
    contentPanel.add(singleLabel, genConstraint(1, y));
    contentPanel.add(singleRoom, genConstraint(2, y));
    y++;

    // standard room
    JLabel standardPriceLabel = new JLabel("Standard Room Price($/day):");
    standardPrice.setColumns(5);
    contentPanel.add(standardPriceLabel, genConstraint(1, y));
    contentPanel.add(standardPrice, genConstraint(2, y));
    y++;
    JLabel standardLabel = new JLabel("Standard room num:");
    standardRoom.setModel(new SpinnerNumberModel(1, 0, 100, 1));
    contentPanel.add(standardLabel, genConstraint(1, y));
    contentPanel.add(standardRoom, genConstraint(2, y));
    y++;

    // family room
    JLabel familyPriceLabel = new JLabel("Family Room Price($/day):");
    familyPrice.setColumns(5);
    contentPanel.add(familyPriceLabel, genConstraint(1, y));
    contentPanel.add(familyPrice, genConstraint(2, y));
    y++;
    JLabel familyLabel = new JLabel("Family room num:");
    familyRoom.setModel(new SpinnerNumberModel(1, 0, 100, 1));
    contentPanel.add(familyLabel, genConstraint(1, y));
    contentPanel.add(familyRoom, genConstraint(2, y));
    y++;

    contentPanel.add(extraPanel, genConstraint(1, y++));
  }

  private void drawActionPanel() {
    actionPanel.setLayout(new BorderLayout());
    JButton cancel = new JButton("Cancel");
    cancel.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        dispose();
      }
    });

    JButton ok = new JButton("OK");
    ok.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String type = typeBox.getSelectedItem().toString();
        String acName = name.getText();
        String acAddr = address.getText();
        String sPrice = singlePrice.getText();
        String stPrice = standardPrice.getText();
        String fPrice = familyPrice.getText();
        int singleNum = (int)singleRoom.getModel().getValue();
        int standardNum = (int)standardRoom.getValue();
        int familyNum = (int)familyRoom.getValue();
        String id = c.attemptCreateAccommodation(type, acName, acAddr, sPrice, stPrice, fPrice);
        if (id != null) {
          if (type.equals("Hotel")) {
            Hotel h = c.getBackend().hotels.get(id);
            h.addRooms("single", singleNum);
            h.addRooms("standard", standardNum);
            h.addRooms("family", familyNum);
            h.setStarLevel((int)starLevel.getValue());
          } else if (type.equals("Cabin")) {
            Cabin h = c.getBackend().cabins.get(id);
            h.addRooms("single", singleNum);
            h.addRooms("standard", standardNum);
            h.addRooms("family", familyNum);
            h.setLandscape(landscape.getText());
            h.setGardenNum((int)gardenNum.getValue());
          } else {
            Apartment h = c.getBackend().apartments.get(id);
            h.addRooms("single", singleNum);
            h.addRooms("standard", standardNum);
            h.addRooms("family", familyNum);
            h.setKitchenNum((int)kitchenNum.getValue());
          }
          c.showPopup("Success!");
          dispose();
        }
      }
    });
    actionPanel.add(cancel, BorderLayout.WEST);
    actionPanel.add(ok, BorderLayout.EAST);
  }

  public CreateAccommodation(ShopController c) {
    this.c = c;
    setTitle("Add New Accommodation");
    setBounds(200, 200, 850, 850);

    drawBasicPanel();
    drawExtraPanel("Hotel");
    drawActionPanel();

    getContentPane().setLayout(new BorderLayout());
    getContentPane().add(contentPanel, BorderLayout.CENTER);
    getContentPane().add(actionPanel, BorderLayout.SOUTH);
  }
}
