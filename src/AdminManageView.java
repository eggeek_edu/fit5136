import java.awt.*;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

public class AdminManageView extends View {
  private static final long serialVersionUID = 1L;
  private JPanel scrollPanel;

  public void setUpLayOut(JPanel panel, String blayout) {
    FlowLayout flowLayout = (FlowLayout) panel.getLayout();
    flowLayout.setAlignment(FlowLayout.LEFT);
    add(panel, blayout);
  }

  public <T extends Accommodation> void displaySearch(ArrayList<T> list, Date s, Date t) {
    scrollPanel.removeAll();
    for(Accommodation p : list) {
      scrollPanel.add(new AccommodationThumbnail(getController(), p, s, t));
    }
    scrollPanel.revalidate();
    scrollPanel.repaint();
  }

  public void createSearchButton(JPanel searcher) {
    // search parameter
    searcher.add(new JLabel("start date:"));
    JSpinner start = new JSpinner();
    start.setModel(new SpinnerDateModel(new Date(), null, null, 1));
    start.setEditor(new JSpinner.DateEditor(start, "yyyy-MM-dd"));
    searcher.add(start);

    searcher.add(new JLabel("days:"));
    JSpinner days = new JSpinner();
    days.setModel(new SpinnerNumberModel(1, 0, 100000, 1));
    searcher.add(days);

    searcher.add(new JLabel("type:"));
    DefaultComboBoxModel model = new DefaultComboBoxModel();
    model.addElement("hotel");
    model.addElement("cabin");
    model.addElement("apartment");
    JComboBox comboBox = new JComboBox(model);
    searcher.add(comboBox);

    searcher.add(new JLabel("name:"));
    JTextField searchName = new JTextField();
    searchName.setColumns(10);
    searcher.add(searchName);

    JButton searchButton = new JButton("Search Report");
    searcher.add(searchButton);
    searchButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        String type = comboBox.getSelectedItem().toString();
        int dur = (int)days.getModel().getValue();
        Date s = (Date)start.getModel().getValue();
        Date t = new Date(s.getTime() + (1000 * 3600 * 24) * dur);
        String name = searchName.getText();
        if (type.equals("hotel")) {
          ArrayList<Hotel> res = getController().adminSearchHotel(s, t, name);
          displaySearch(res, s, t);
        }
        if (type.equals("cabin")) {
          ArrayList<Cabin> res = getController().adminSearchCabin(s, t, name);
          displaySearch(res,s ,t);
        }
        if (type.equals("apartment")) {
          ArrayList<Apartment> res = getController().adminSearchApartment(s, t, name);
          displaySearch(res,s, t);
        }
      }
    });
  }

  private void createLogoutButton(JPanel panel) {
    JButton logoutButton = new JButton("Log out");
    logoutButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        getController().logout();
      }
    });
    panel.add(logoutButton, BorderLayout.WEST);
  }

  private void addCreateItemButton(JPanel panel) {
    JButton createItem = new JButton("Add");
    createItem.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        CreateAccommodation.display(getController());
      }
    });
    panel.add(createItem);
  }

  private void createBottomPanel(JPanel bottom) {
    bottom.setLayout(new BorderLayout());
    createLogoutButton(bottom);
    JPanel actions = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    addCreateItemButton(actions);
    bottom.add(actions, BorderLayout.EAST);
  }

  public AdminManageView() {
    setLayout(new BorderLayout(0, 0));
    JPanel searcher = new JPanel();
    setUpLayOut(searcher, BorderLayout.NORTH);
    createSearchButton(searcher);

    JPanel bottom = new JPanel();
    createBottomPanel(bottom);
    add(bottom, BorderLayout.SOUTH);

    scrollPanel = new JPanel();
    JScrollPane scroll = new JScrollPane(scrollPanel);
    scrollPanel.setLayout(new GridLayout(0, 3, 0, 0));
    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    add(scroll, BorderLayout.CENTER);
  }

  public void initialize() {}
}
