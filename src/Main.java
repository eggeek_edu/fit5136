public class Main {

  public static void main(String[] args) {
    MyModel shopModel = new MyModel();
    ShopController c = new ShopController(shopModel);
    c.init();
  }
}
