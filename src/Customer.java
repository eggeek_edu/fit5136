import java.util.*;

/**
 * <pre>
 * This class represents a customer, it holds all of the data that the store needs to know about someone.
 * Whilst this information is not sufficient to place an order in real life, it is detailed enough to serve as a good learning example.
 * </pre>
 */
public class Customer extends Account {
  public String name;
  public String address;
  public String cardNumber;
  public Cart cart;
  public String phoneNumber;
  public ArrayList<String> orderIDs;
  public ArrayList<String> paymentIDs;
  public ArrayList<String> refundIDs;

  public Customer(String username, String password, String name, String address, String cardNumber, String phoneNumber) {
    super(username, password);
    this.name = name;
    this.address = address;
    this.cardNumber = cardNumber;
    this.phoneNumber = phoneNumber;
    orderIDs = new ArrayList<>();
    paymentIDs = new ArrayList<>();
    refundIDs = new ArrayList<>();
    cart = new Cart();
  }

  private void loadHtmlHeader(ArrayList<String> out) {
    out.add("<html>");
    out.add("<style>th, td {border: 1px solid black;}</style>");
    out.add("<caption>Payment history</caption>");
    out.add("<table style=\"width:100%\">");
    out.add("<tr>");
    out.add("<th>start date</th>");
    out.add("<th>end date</th>");
    out.add("<th>amount</th>");
    out.add("</tr>");
  }
  public String toPaymentHtml(MyModel b) {
    ArrayList<String> out = new ArrayList<>();
    loadHtmlHeader(out);
    for (String i: paymentIDs) {
      PaymentRecord p = b.payments.get(i);
      out.add("<tr>");
      out.add(String.format("<td>%s</td>", p.startDate.toString()));
      out.add(String.format("<td>%s</td>", p.endDate.toString()));
      out.add(String.format("<td>%.2f</td>", p.amount));
      out.add("</tr>");
    }

    out.add("</table>");
    out.add("</html>");
    return String.join("", out);

  }
  public String toRefundHtml(MyModel b) {
    ArrayList<String> out = new ArrayList<>();
    loadHtmlHeader(out);
    for (String i: refundIDs) {
      RefundRecord p = b.refunds.get(i);
      out.add("<tr>");
      out.add(String.format("<td>%s</td>", p.startDate.toString()));
      out.add(String.format("<td>%s</td>", p.endDate.toString()));
      out.add(String.format("<td>%.2lf</td>", p.amount));
      out.add("</tr>");
    }

    out.add("</table>");
    out.add("</html>");
    return String.join("", out);
  }
}
